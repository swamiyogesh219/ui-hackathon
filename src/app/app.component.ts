import { Component, ViewChild } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogComponentComponent } from 'src/components/dialog-component/dialog-component.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild('myModal') myModal:any;
  constructor(private dialog: MatDialog){}
  tableSelected:boolean = true;
  title = 'hackathon';
  refresh=1;

  catchEvent =  (event:string) => {
    if(event == "chart"){
      this.tableSelected = false;
    }else{
      this.tableSelected = true;
    }
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.height = '100vw';
    dialogConfig.width = '25vw';
    dialogConfig.position = {
      right:"0px"
    }
    this.dialog.open(DialogComponentComponent, dialogConfig).afterClosed().subscribe(()=>this.refreshParent())
}

refreshParent  = () => {
  this.refresh++;
}

  openModel() {
    this.openDialog();
 }

  closeModel() {
    this.myModal.nativeElement.className = 'modal hide';
 }
}
