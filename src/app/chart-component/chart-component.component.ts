import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartComponent } from 'ng-apexcharts';
import { ResourceServiceService } from 'src/services/resource-service.service';


@Component({
  selector: 'app-chart-component',
  templateUrl: './chart-component.component.html',
  styleUrls: ['./chart-component.component.scss']
})
export class ChartComponentComponent implements OnInit {
  @ViewChild("chart", { static: false }) chart!: ChartComponent;
  loading = true;
  tableData:any;
  series:any = [];
  public chartOptions: any;
  constructor(private resources:ResourceServiceService) { 
    this.chartOptions = {
      series: [],
      chart: {
        width: 380,
        type: "pie"
      },
      labels: [],
    };
    resources.getItems().subscribe( (res) => {
      this.tableData = res;
      this.getParticularColumnData(3);
    });
    
    
  }
  ngOnInit(): void {
  }

  getParticularColumnData = (name:any) => {
    this.loading = true;
    this.series = [];
    this.tableData.forEach((element:object) => {
      const val = Object.values(element)[name];
      const upperval = val.toUpperCase();
      this.series.push(upperval);
    });

    let countLetters = this.series.reduce((m:any,n:any)=>({...m, [n]:-~m[n]}),{});
    let set:any = [];
    let label:any =[];

    Object.keys(countLetters).forEach(key => {
      label.push(key);
      set.push(countLetters[key]);
    });
    
    this.chartOptions = {
      series: set,
      chart: {
        width: 380,
        type: "pie"
      },
      labels:label,
    };
    this.loading = false;
  }
}
