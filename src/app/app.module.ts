import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HeaderComponentComponent } from 'src/components/header-component/header-component.component';
import { TableComponentComponent } from 'src/components/table-component/table-component.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableItemComponentComponent } from 'src/components/table-item-component/table-item-component.component';
import { MatIconModule } from '@angular/material/icon';
import { ChartComponentComponent } from './chart-component/chart-component.component';
import { NgApexchartsModule } from "ng-apexcharts";
import { DialogComponentComponent } from 'src/components/dialog-component/dialog-component.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponentComponent,
    TableComponentComponent,
    TableItemComponentComponent,
    ChartComponentComponent,
    DialogComponentComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatIconModule,
    NgApexchartsModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponentComponent]
})
export class AppModule { }
