import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ResourceServiceService {

  dataChange= new EventEmitter();

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private apiUrl = 'https://63037d409eb72a839d824580.mockapi.io/Vehicle';

  constructor(private http: HttpClient) {}

  getItems = () => {
    return this.http.get(this.apiUrl);
  }

  addVehicle = (item:any) => {
    this.http.post(this.apiUrl, item, this.httpOptions).subscribe((res:any)=>{
      console.log(res);
    });
    this.dataChange.emit();
  }

  deleteVehicle = (id:any) => {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }

  getDataChange = () => {
    return this.dataChange;
  }
}
