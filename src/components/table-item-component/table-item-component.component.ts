import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: '[app-table-item-component]',
  templateUrl: './table-item-component.component.html',
  styleUrls: ['./table-item-component.component.scss']
})
export class TableItemComponentComponent implements OnInit {
  @Input() item:any;
  @Input() checkAll:any;
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  // @Output() delete:EventEmitter<any> = new EventEmitter();
  constructor() {}

  ngOnInit(): void {
  }

  getCheckedStatus = () => {
    console.log(this.checkAll);
    return this.checkAll;
  }

  deleteVehicle = (id:any) => {
    // console.log(id);
    this.delete.emit(id);
  }
}
