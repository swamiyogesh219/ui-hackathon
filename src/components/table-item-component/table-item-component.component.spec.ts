import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableItemComponentComponent } from './table-item-component.component';

describe('TableItemComponentComponent', () => {
  let component: TableItemComponentComponent;
  let fixture: ComponentFixture<TableItemComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableItemComponentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TableItemComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
