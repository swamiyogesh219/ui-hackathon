import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.scss']
})
export class HeaderComponentComponent implements OnInit {
  @ViewChild('chart') chart:any;
  @ViewChild('data') data:any;
  @Output() selectedIcon = new EventEmitter<string>();
  @Output() openIt = new EventEmitter<string>();
  constructor() {
   }

  @Output() myClick = new EventEmitter();

  onMyClick() {
      this.myClick.emit();
  }

  ngOnInit(): void {
  }

  selectData = () => {
    this.data.nativeElement.style.backgroundColor = "#d8ecff";
    this.chart.nativeElement.style.backgroundColor = "#ffffff";
    this.selectedIcon.emit("table");
  } 
  
  selectChart = () => {
    this.chart.nativeElement.style.backgroundColor = "#d8ecff";
    this.data.nativeElement.style.backgroundColor = "#ffffff";
    this.selectedIcon.emit("chart");
  }

  openModel = () => {
    this.openIt.emit("open");
    console.log('emitted');
  }
}
