import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ResourceServiceService } from 'src/services/resource-service.service';

@Component({
  selector: 'app-dialog-component',
  templateUrl: './dialog-component.component.html',
  styleUrls: ['./dialog-component.component.scss']
})
export class DialogComponentComponent implements OnInit {
  @Output() added = new EventEmitter();
  carForm = new FormGroup({
    name: new FormControl(''),
    manufacturer: new FormControl(''),
    model:new FormControl(''),
    fuel:new FormControl(''),
    colour:new FormControl(''),
    price:new FormControl(''),
    currency:new FormControl(''),
    city:new FormControl(''),
    country:new FormControl('')
  });

  constructor(private dialogRef: MatDialogRef<DialogComponentComponent>, private resources: ResourceServiceService){}

  ngOnInit() {
  }

  close = () => {
      this.dialogRef.close();
  }

  save = () => {
    this.resources.addVehicle(this.carForm.value);
    this.added.emit();
    this.carForm.reset();
    this.dialogRef.close();
    alert('Vehicle Information Added Successfully');
  }

}
