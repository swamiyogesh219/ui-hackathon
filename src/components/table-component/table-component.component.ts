import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ResourceServiceService } from 'src/services/resource-service.service';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-table-component',
  templateUrl: './table-component.component.html',
  styleUrls: ['./table-component.component.scss']
})
export class TableComponentComponent implements OnInit,OnChanges {
  @Input() refresh:any;
  tableData:any;
  loading = true;
  subscription:any;
  checkStatus = false;
  constructor(private resources:ResourceServiceService,private changeDetectorRefs: ChangeDetectorRef) { 
    resources.getItems().subscribe( (res) => {
      this.tableData = res;
      this.loading = false;
      this.changeDetectorRefs.detectChanges();
    });
    this.subscription = this.resources.getDataChange();
  }

  ngOnInit(): void {
  }

  changeCheckStatus = () => {
    this.checkStatus = !this.checkStatus;
  }

  delete = (event:any) => {
    this.resources.deleteVehicle(event).subscribe((res:any)=>{
      this.resources.getItems().subscribe((res:any)=>{
        this.tableData = res;
      })
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.resources.getItems().subscribe((res:any)=>{
      this.tableData = res;
    })
  }
}
